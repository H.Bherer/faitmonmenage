const { AdminModel } = require('../models/adminModel')

module.exports = async (req, res, next) => {
    if (!req.session.authenticated) {
        res.redirect('/login');
    } else {
        res.locals.admin = await AdminModel.findOne({ email: req.session.email })
        next();
    }
}