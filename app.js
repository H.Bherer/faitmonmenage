var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var cors = require('cors');
var logger = require('morgan');
var mongoose = require('mongoose');
const hbs = require('hbs');
require('dotenv').config()
const exSession = require('express-session')

mongoose.connect(
  "mongodb://localhost:27017/faitmonmenage", {
      useNewUrlParser: true,
      useUnifiedTopology: true
  }
);

var db = mongoose.connection;
db.on("error", console.error.bind(console, "erreur de connection:"));



var indexRouter = require('./routes/cms');
var apiRouter = require('./routes/api');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(cors())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(exSession({
  secret: process.env.SESSION_TOKEN,
  resave: false,
  saveUninitialized: false
})
);

app.use('/', indexRouter);
app.use('/api', apiRouter);

hbs.registerHelper('if_eq', function(a, b, opts) {
  if (a == b) {
      return opts.fn(this);
  } else {
      return opts.inverse(this);
  }
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
