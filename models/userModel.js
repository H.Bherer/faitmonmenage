const Mongoose = require("mongoose");
let Schema = Mongoose.Schema;

const UserSchema = new Schema({
    first_name: String,
    last_name: String,
    email: String,
    postal_code: String,
    phone: String,
    type: String,
    hire: String
});

exports.userModel = Mongoose.model("users", UserSchema);