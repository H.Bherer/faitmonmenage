var Mongoose = require("mongoose");

var Schema = Mongoose.Schema;
let EmployeSchema = new Schema({
    nom: String,
    prenom: String,
    courriel: String,
    adresse: String,
    telephone: String,
    etat: String,
    hire: String
});

exports.EmployeModel = Mongoose.model("Employe", EmployeSchema);


